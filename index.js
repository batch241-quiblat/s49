// Mock Database
//let posts = [];

// Post ID
//let count = 1;

fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((data) => showPosts(data));

// Reactive DOM with JSON (CRUD Operation)

// ADD POST DATA
// This will trigger an event that will add a new post in our mock database upon clicking the "create" button.
document.querySelector("#form-add-post").addEventListener('submit', (e) => {
	// Prevents the default behavior of an event
	// To prevent the page from reloading
	e.preventDefault();
	// Adds an element in the end of an array and returns array's length\

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	}).then((response) => response.json()).then((data) => {
		console.log(data);
		alert("Post successfully added!");
		document.querySelector("#txt-title").value = null
		document.querySelector("#txt-body").value = null
	});

});


// RETRIEVE POST
const showPosts = (posts) => {
	// variable that will contain all the posts
	let postEntries = "";
	// Looping through array items
	posts.forEach((post) => {
		// addition assignment (+=) operator adds the value of the right operand and assigns the result to the variable
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});
	// To check what is stored in the postEntries variables
	console.log(postEntries);
	// To assign the value of postEntries to the element with "div-post-entries" id
	document.querySelector('#div-post-entries').innerHTML = postEntries
};

// EDIT POST (Edit Button)
const editPost = (id) => {

	// The function first uses querySelector() method to get the element with id
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector("#txt-edit-id").value = id
	document.querySelector("#txt-edit-title").value = title
	document.querySelector("#txt-edit-body").value = body

	document.querySelector("#btn-submit-update").removeAttribute('disabled');
}

// UPDATE POST (Update Button)
document.querySelector("#form-edit-post").addEventListener('submit', (e) => {
	
	e.preventDefault();
	
	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: 'PUT',
		body:JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert("Post successfully updated!");
		document.querySelector("#txt-edit-id").value = null
		document.querySelector("#txt-edit-title").value = null
		document.querySelector("#txt-edit-body").value = null

		document.querySelector("#btn-submit-update").setAttribute('disabled', true);

	})
});

const deletePost = (id) => {

	
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: 'DELETE'
	})
	
	const data = document.querySelector(`#post-${id}`);
	console.log(data);
	data.remove();
	alert("Post successfully deleted!");

}





